'use strict';

const db = require('../models/index')

module.exports = {
    up: (queryInterface, Sequelize) => {
        // Use for dev purpose, create proper migration file for production use.
        return db.sequelize.sync({ alter: true });
  },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('Accounts');
        await queryInterface.dropTable('Weights'); 
        return queryInterface.dropTable('Activities');;
  }
};
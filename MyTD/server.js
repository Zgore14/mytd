'use strict';

const express = require('express');
const expressHandlebars = require('express-handlebars');
const bodyParser = require('body-parser');
const session = require('express-session');
const middlewares = require('./middlewares');
const favicon = require('serve-favicon');
const path = require('path');

/* --- General --- */
const app = express();
app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));

/* --- Session --- */
app.use(session({
    'secret': 'DEV14SECR3T',
    resave: false,
    saveUninitialized: true
}));

/* --- Pages --- */
const hbs = expressHandlebars.create({
    extname: 'hbs',
    defaultLayout: 'main.hbs',
    layoutsDir: __dirname + '/views/layouts/',
    partialsDir: __dirname + '/views/partials/'
});
// TODO: Think to install Bulma for css
app.use(express.static("public"));
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');

/* --- Middlewares --- */
app.use(middlewares.isAuthenticated);

/* --- Routes ---*/
app.use(bodyParser.urlencoded({ extended: false }));
require('./routes')(app);

/* --- API Routes ---*/
app.use(bodyParser.json());
const apiRoutes = require('./api/routes');
app.use('/api', apiRoutes);

var server = app.listen(1337, function () {
    var host = server.address().address;
    var port = server.address().port;
    if (host === "::") {
        host = "localhost";
    }
    console.log('MyTD app listening at http://%s:%s', host, port);
});
    
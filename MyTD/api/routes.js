const db = require('../models');
const express = require('express');
const router = express.Router();
const auth = require('../authentication');
const jwt = require('jsonwebtoken');

router.post('/test', auth.ensureToken, function(req, res) {
    res.send({ msg: "API is working" });
});

// AUTH

router.post('/tokens', async function (req, res) {
    if (req.body.grant_type === "password") {
        username = req.body.username;
        password = req.body.password;

        if (username !== "undefined" && password !== "undefined") {
            try {
                const account = await db.Account.findOne({
                    where: {
                        username: username
                    }
                });

                // TODO: Put the secret key in a file if used for production
                if (account) {
                    const valid = await account.validPassword(password);
                    if (valid) {
                        const IDToken = jwt.sign({
                            sub: account.id,
                            preferred_username: account.username
                        }, 'secretkey', { expiresIn: '1d' });

                        const AccessToken = jwt.sign({
                            userId: account.id
                        }, 'secretaccesskey', { expiresIn: '1h' });

                        return res.send({
                            access_token: AccessToken,
                            token_type: "Bearer",
                            id_token: IDToken
                        });
                    }
                }

            }
            catch (e) {
                console.log(e);
                return res.status(401).send({ "error": "invalid_grant" });
            }

        }

    }
    res.status(400).send({ "error": "invalid_grant" });
});

// WEIGHTS

router.get('/weights', auth.ensureToken, function (req, res) {
    res.status(501).send();
});

router.post('/weights', auth.ensureToken, function (req, res) {
    accountId = req.body.userId;
    value = req.body.weight;
    time = req.body.time;
    if (typeof accountId === "undefined"
        || typeof value === "undefined"
        || typeof time === "undefined"
        || value && value < 0) {
        return res.status(400).send();
    }
    db.Weight.create({ value: value, time: time, accountId: accountId})
        .then(function (weight) {
            return res.status(204).send();
        })
        .catch(function (err) {
            console.log(err);
            return res.status(400).send();
        });
});

router.put('/weights', auth.ensureToken, function (req, res) {
    res.status(501).send();
});

router.delete('/weights', auth.ensureToken, function (req, res) {
    res.status(501).send();
});

// ACTIVITIES

router.get('/activities', auth.ensureToken, function (req, res) {
    res.status(501).send();
});

router.post('/activities', auth.ensureToken, function (req, res) {
    accountId = req.body.userId;
    title = req.body.title;
    start = req.body.start;
    stop = req.body.stop;
    description = req.body.description;

    if (typeof accountId === "undefined"
        || typeof title === "undefined"
        || title && title.length === 0
        || typeof start === "undefined"
        || typeof stop === "undefined"
        || start && stop && (new Date(stop) < new Date(start))
        || typeof description === "undefined") {
        return res.status(400).send();
    }
    db.Activity.create({
        title: title, start: start, stop: stop,
        description: description, accountId: accountId
    })
        .then(function (activity) {
            return res.status(204).send();
        })
        .catch(function (err) {
            console.log(err);
            return res.status(400).send();
        });
});

router.put('/activities', auth.ensureToken, function (req, res) {
    res.status(501).send();
});

router.delete('/activities', auth.ensureToken, function (req, res) {
    res.status(501).send();
});


module.exports = router;
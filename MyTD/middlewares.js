﻿'use strict';

module.exports = {

    isAuthenticated: function (req, res, next) {

        var endpoint = req.path;

        if (endpoint === "/accounts/create") {
            return next();
        }

        if (/\/api\/.*/.test(endpoint)) {
            return next();
        }

        if (!req.session.authenticated
            && (endpoint === "/dashboard"
            || /\/accounts(\/.*)*/.test(endpoint)
            || /\/weights(\/.*)*/.test(endpoint)
            || /\/activities(\/.*)*/.test(endpoint))) {
            res.status(403).redirect('/login');
            return;
        }

        if (req.session.authenticated
            && (endpoint === "/login"
                || endpoint === "/register")
                || endpoint === "/account/create") {
            res.redirect("/dashboard");
            return;
        }

        next();
    }

};
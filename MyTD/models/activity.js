'use strict';
module.exports = (sequelize, DataTypes) => {
    const Activity = sequelize.define('Activity', {
        title: DataTypes.STRING,
        start: DataTypes.DATE,
        stop: DataTypes.DATE,
        description: DataTypes.STRING
    }, {});
    Activity.associate = function (models) {
        Activity.belongsTo(models.Account, {
            foreignKey: 'accountId',
            onDelete: 'CASCADE'
        });
    };
    return Activity;
};
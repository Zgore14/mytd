'use strict';

const bcrypt = require("bcrypt");

module.exports = (sequelize, DataTypes) => {

    var Account = sequelize.define('Account', {
        username: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        city: DataTypes.STRING,
        born: DataTypes.DATEONLY
    }, {});

    Account.associate = function (models) {
        Account.hasMany(models.Activity, {
            foreignKey: 'accountId',
            onDelete: 'CASCADE'
        });

        Account.hasMany(models.Weight, {
            foreignKey: 'accountId',
            onDelete: 'CASCADE'
        });
    };

    Account.generateHash = function (password) {
        return bcrypt.hash(password, bcrypt.genSaltSync(8));
    };

    Account.prototype.validPassword = function (password) {
        return bcrypt.compare(password, this.password);
    };

    return Account;
};
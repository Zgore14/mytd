'use strict';
module.exports = (sequelize, DataTypes) => {
    const Weight = sequelize.define('Weight', {
        value: DataTypes.INTEGER,
        time: DataTypes.DATE
    }, {});
    Weight.associate = function (models) {
        Weight.belongsTo(models.Account, {
            foreignKey: 'accountId',
            onDelete: 'CASCADE'
        });
    };
    return Weight;
};
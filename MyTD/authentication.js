let jwt = require('jsonwebtoken');

module.exports.ensureToken = function (req, res, next) {
    var bearerHeader = req.headers["authorization"];

    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];
        jwt.verify(bearerToken, 'secretaccesskey', (err, result) => {
            if (err) {
                res.sendStatus(401);
            }
            else {
                res.locals.apiUserId = result.userId;
                next();
            }
        });
    }
    else {
        res.sendStatus(401);
    }
}
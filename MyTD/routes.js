﻿const db = require('./models');

module.exports = function (app) {

    // GET

    app.get('/', function (req, res) {
        sess = req.session;
        if (sess.authenticated) {
            res.render('home.hbs', { authenticated: sess.authenticated });
        } else {
            res.render('home.hbs');
        }
    });

    app.get('/about', function (req, res) {
        sess = req.session;
        res.render('about.hbs', {
            authenticated: sess.authenticated
        });
    });

    app.get('/contact', function (req, res) {
        sess = req.session;
        res.render('contact.hbs', {
            authenticated: sess.authenticated
        });
    });

    app.get('/accounts', function (req, res) {
        sess = req.session;

        db.Account.findAll()
            .then((accounts) => {
                res.render('accounts', {
                    authenticated: sess.authenticated,
                    accounts: accounts
                });
            })
            .catch((err) => {
                console.log('There was an error querying accounts', JSON.stringify(err));
                res.send(err);
            });
    });

    app.get('/accounts/:id', function (req, res) {
        sess = req.session;
        id = req.params.id;
        db.Account.findById(id)
            .then(async (account) => {
                try {
                    const weights = await db.Weight.findAll({ where: { accountId: id } });
                    const activities = await db.Activity.findAll({ where: { accountId: id } });

                    res.render('account-details', {
                        weights: weights,
                        activities: activities,
                        authenticated: sess.authenticated,
                        account: account
                    });
                }
                catch (e) {
                    console.log(e);
                }
                
            })
            .catch((err) => {
                console.log('There was an error querying account', JSON.stringify(err));
                res.statusMessage = "There was an error querying account";
                res.status(400).redirect('/dashboard');
            });
    });

    app.get('/dashboard', async function (req, res) {
        sess = req.session;
        username = "Anonymous";
        if (sess.username) {
            username = sess.username;
        }

        try {
            const weights = await db.Weight.findAll({
                where: {
                    accountId: sess.currentAccountId
                },
                order: [['createdAt', 'DESC']]
            });

            const activities = await db.Activity.findAll({
                where: {
                    accountId: sess.currentAccountId
                }
            });

            res.render('dashboard.hbs', {
                authenticated: sess.authenticated,
                username: username,
                weights: weights,
                activities: activities
            });

        } catch (err) {
            console.log(err);
            res.render('dashboard.hbs', {
                authenticated: sess.authenticated,
                username: username
            });
        }
    });

    app.get('/register', function (req, res) {
        res.render('register.hbs');
    });

    app.get('/login', function (req, res) {
        res.render('login.hbs');
    });

    app.get('/logout', function (req, res) {
        req.session.destroy(function (err) {
            if (err) {
                console.log(err);
            } else {
                res.redirect('/');
            }
        });
    });

    app.get('/weights/create', function (req, res) {
        sess = req.session;
        res.render('weight-creation', {
            authenticated: sess.authenticated
        });
    });

    app.get('/activities/create', function (req, res) {
        sess = req.session;
        res.render('activity-creation', {
            authenticated: sess.authenticated
        });
    });

    // POST

    app.post('/accounts/create', function (req, res) {
        email = req.body.email;
        username = req.body.username;
        password = req.body.password;
        city = req.body.city;
        born = req.body.born;

        err = false;
        if (typeof email === "undefined" || email !== null && email.length === 0) {
            err = true;
            err.prefix = "email";
        } else if (typeof username === "undefined" || username !== null && username.length === 0) {
            err = true;
            err.prefix = "username";
        } else if (typeof password === "undefined" || password !== null && password.length === 0) {
            err = true;
            err.prefix = "password";
        } else if (typeof city === "undefined" || city !== null && city.length === 0) {
            err = true;
            err.prefix = "city";
        } else if (typeof born === "undefined") {
            err = true;
            err.prefix = "born";
        }

        if (err) {
            console.log(err);
            res.statusMessage = err.prefix + " field missing or empty";
            res.status(400).redirect('/register');
            return;
        }

        db.Account.generateHash(password)
            .then(function (hashPassword) {
                db.Account.create({ email: email, username: username, password: hashPassword, city: city, born: born })
                    .then((account) => {
                        res.statusMessage = "Account " + account.username + " created";
                        res.redirect('/login');
                    })
                    .catch((err) => {
                        console.log(err);
                        res.statusMessage = "Account creation failed";
                        return res.status(400).redirect('/register');
                    });
            })
            .catch(function (err) {
                console.log(err);
                res.statusMessage = "Account creation failed";
                res.status(400).redirect('/register');
            });


    });

    app.post('/weights/create', function (req, res) {
        sess = req.session;
        value = req.body.value;
        time = req.body.time;

        console.log(time);
        console.log(value);

        if (typeof value === "undefined"
            || typeof time === "undefined"
            || value && value < 0) {
            return res.status(400).redirect('/weights/create');
        }

        db.Weight.create({ value: value, time: time, accountId: sess.currentAccountId })
            .then(function (weight) {
                return res.redirect('/dashboard');
            })
            .catch(function (err) {
                console.log(err);
                return res.status(400).redirect('/weights/create');
            });
    });

    app.post('/activities/create', function (req, res) {
        sess = req.session;
        title = req.body.title;
        start = req.body.start;
        stop = req.body.stop;
        description = req.body.description;

        if (typeof title === "undefined"
            || title && title.length === 0
            || typeof start === "undefined"
            || typeof stop === "undefined"
            || start && stop && new Date(stop) < new Date(start)
            || typeof description === "undefined") {
            return res.status(400).redirect('/activities/create');
        }
        db.Activity.create({
            title: title, start: start, stop: stop,
            description: description, accountId: sess.currentAccountId
        })
            .then(function (activity) {
                return res.redirect('/dashboard');
            })
            .catch(function (err) {
                console.log(err);
                return res.status(400).redirect('/activities/create');
            });
    });

    app.post('/login', function (req, res) {
        username = req.body.username;
        password = req.body.password;

        sess = req.session;
        sess.username = username;

        db.Account.findOne({
            where: {
                username: username
            }
        })
            .then(function (account) {

                account.validPassword(password)
                    .then(function (valid) {
                        if (account && valid) {
                            sess.currentAccountId = account.id;
                            sess.authenticated = true;
                            res.redirect('/dashboard');
                        } else {
                            sess.authenticated = false;
                            res.status(401).redirect('/login');
                        }
                    })
                    .catch(function (err) {

                    });
            })
            .catch((err) => {
                console.log(err);
                sess.authenticated = false;
                res.status(401).redirect('/login');
            });


    });

};
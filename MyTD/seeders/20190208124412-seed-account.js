'use strict';

const bcrypt = require("bcrypt");

module.exports = {
    up: async (queryInterface, Sequelize) => {
        /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.
    
          Example:
          return queryInterface.bulkInsert('People', [{
            name: 'John Doe',
            isBetaMember: false
          }], {});
        */

        try {

            await queryInterface.bulkInsert('Accounts', [{
                username: "Zgore",
                email: "zgore@gmail.com",
                // /!\ Look in account model in order to have the same hash salt
                password: bcrypt.hashSync("test", bcrypt.genSaltSync(8)),
                city: "Soisy",
                born: new Date(1997, 6, 14),
                createdAt: new Date(),
                updatedAt: new Date()
            }, {
                username: "Oraekia",
                email: "oraekia@yahoo.com",
                password: bcrypt.hashSync("test", bcrypt.genSaltSync(8)),
                city: "Fontenay",
                born: new Date(1997, 10, 14),
                createdAt: new Date(),
                updatedAt: new Date()
            }], {});

            var accounts = await queryInterface.sequelize.query(
                `SELECT id from ACCOUNTS;`
            );

            accounts = accounts[0];

            await queryInterface.bulkInsert('Weights', [{
                value: 70,
                time: new Date(2018, 9, 10),
                accountId: accounts[0].id,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                value: 68,
                time: new Date(2019, 1, 4),
                accountId: accounts[0].id,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                value: 67,
                time: new Date(2019, 3, 1),
                accountId: accounts[0].id,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                value: 78,
                time: new Date(2019, 1, 28),
                accountId: accounts[1].id,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                value: 78,
                time: new Date(2019, 2, 15),
                accountId: accounts[1].id,
                createdAt: new Date(),
                updatedAt: new Date()
            }], {});

            return await queryInterface.bulkInsert("Activities", [{
                title: "Volleyball",
                start: new Date(),
                stop: new Date(),
                description: "2 hours at Campus Arena",
                accountId: accounts[0].id,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                title: "Workout",
                start: new Date(),
                stop: new Date(),
                description: "1 hour at Nordic Wellness",
                accountId: accounts[0].id,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                title: "Volleyball",
                start: new Date(),
                stop: new Date(),
                description: "2 hours at Campus Arena",
                accountId: accounts[1].id,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                title: "Workout",
                start: new Date(),
                stop: new Date(),
                description: "1 hour at Nordic Wellness",
                accountId: accounts[1].id,
                createdAt: new Date(),
                updatedAt: new Date()
            }
            ], {});

        }
        catch (e) {
            console.log(e);
        }

    },

    down: async (queryInterface, Sequelize) => {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.
    
          Example:
          return queryInterface.bulkDelete('People', null, {});
        */

        await queryInterface.bulkDelete("Weights", null, {});

        await queryInterface.bulkDelete("Activities", null, {});

        return await queryInterface.bulkDelete("Accounts", null, {});
    }
};

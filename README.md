# MyTD

# Prerequisites

Install node.js and npm

Go to the project directory:
```bash
cd MyTD/
```

# Install dependencies
```bash
npm install
```

# Initialize database
```bash
./node_modules/.bin/sequelize db:migrate
```

# Launch the server
```bash
node .
```